using System;

namespace Class
{
    public class Station
    {
        public delegate void LavageStation(Voiture p_voiture);
        public static void LavageFaible(Voiture p_voiture)
        {
            p_voiture.DegreSalisure -= 2;
        }
        public static void LavageNormal(Voiture p_voiture)
        {
            p_voiture.DegreSalisure -= 4;
        }
        public static void LavageIntense(Voiture p_voiture)
        {
            p_voiture.DegreSalisure -= 7;
        }
        public static void LavageDeluxe(Voiture p_voiture)
        {
            p_voiture.DegreSalisure -= 9;
        }
    }
}
