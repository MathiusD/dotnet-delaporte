﻿using System;

namespace Class
{
    public class MaClass
    {
        public MaClass()
        {
            Console.WriteLine("Constructeur");
        }

        ~MaClass()
        {
            Console.WriteLine("Destructeur");
        }
    }
}
