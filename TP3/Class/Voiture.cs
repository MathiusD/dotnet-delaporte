using System;

namespace Class
{
    public class Voiture : Vehicule
    {
        private byte m_degreSalisure;
        public sbyte DegreSalisure
        {
            get => (sbyte)m_degreSalisure;
            set
            {
                if (value > byte.MinValue)
                {
                    if (value > 10)
                    {
                        m_degreSalisure = 10;
                    }
                    else
                    {
                        m_degreSalisure = (byte)value;
                    }
                }
                else
                {
                    m_degreSalisure = byte.MinValue;
                }
            }
        }
        public Voiture(ushort p_nbPassenger, uint p_nbCapacity, byte p_degreSalisure) : base(TypeVehicule.Voiture, p_nbPassenger, p_nbCapacity)
        {
            m_degreSalisure = p_degreSalisure;
        }

        public Voiture(ushort p_nbPassenger, uint p_nbCapacity): this(p_nbPassenger, p_nbCapacity, 0) {}

        public Voiture(uint p_nbCapacity): this(0, p_nbCapacity, 0) {}

        public override sealed String SeDeplacer()
        {
            return "Roule";
        }
    }
}
