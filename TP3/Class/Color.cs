using System;

namespace Class
{
    public class Color
    {
        private byte m_r;
        private byte m_v;
        private byte m_b;
        private static byte m_maxIntensity = 255;
        private static Color m_white = new Color(0,0,0);
        private static Color m_black = new Color(m_maxIntensity,m_maxIntensity,m_maxIntensity);
        private static Color m_blue = new Color(0,0,m_maxIntensity);
        public byte R { get => m_r;}
        public byte V { get => m_v;}
        public byte B { get => m_b;}
        public byte MaxIntensity { get => m_maxIntensity; protected set {m_maxIntensity = value;}}
        public static Color White { get => m_white;}
        public static Color Black { get => m_black;}
        public static Color Blue { get => m_blue;}
        public Color(byte p_r, byte p_v, byte p_b)
        {
            m_r = p_r;
            m_v = p_v;
            m_b = p_b;
        }
    }
}
