using System;

namespace Class
{
    public abstract class Vehicule
    {
        private TypeVehicule m_type;
        private ushort m_nbPassenger;
        private uint m_nbCapacity;
        public TypeVehicule Type { get => m_type;}
        public ushort NbPassenger { get => m_nbPassenger; set {m_nbPassenger = value;} }
        public uint NbCapacity { get => m_nbCapacity;}
        public Vehicule(TypeVehicule p_type, ushort p_nbPassenger, uint p_nbCapacity)
        {
            m_type = p_type;
            m_nbPassenger = p_nbPassenger;
            m_nbCapacity = p_nbCapacity;
        }

        public Vehicule(TypeVehicule p_type, uint p_nbCapacity) : this(p_type, 0, p_nbCapacity) {}

        public abstract String SeDeplacer();

        public virtual String GetInfosTechniques()
        {
            return String.Format("Type : {0}\nNbPassenger : {1}\nNbCapacity : {2}\n", m_type, m_nbPassenger, m_nbCapacity);
        }
    }
}
