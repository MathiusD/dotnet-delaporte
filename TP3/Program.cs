﻿using System;
using Class;

namespace TP3
{
    class Program
    {
        static void Main(string[] args)
        {
            MaClass v_ma_class = new MaClass();
            v_ma_class = null;
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            Color v_a = new Color(45,45,45);
            Console.WriteLine(v_a.R);
            Console.WriteLine(v_a.V);
            Console.WriteLine(v_a.B);
            Console.WriteLine(v_a.MaxIntensity);
            Console.WriteLine(Color.Black.R);
            Console.WriteLine(Color.Black.V);
            Console.WriteLine(Color.Black.B);
            Console.WriteLine(Color.White.R);
            Console.WriteLine(Color.White.V);
            Console.WriteLine(Color.White.B);
            Console.WriteLine(Color.Blue.R);
            Console.WriteLine(Color.Blue.V);
            Console.WriteLine(Color.Blue.B);
            Color v_b = new Color(75,75,75);
            Console.WriteLine(v_b.R);
            Console.WriteLine(v_b.V);
            Console.WriteLine(v_b.B);
            Console.WriteLine(v_b.MaxIntensity);
            Console.WriteLine(Color.Black.R);
            Console.WriteLine(Color.Black.V);
            Console.WriteLine(Color.Black.B);
            Console.WriteLine(Color.White.R);
            Console.WriteLine(Color.White.V);
            Console.WriteLine(Color.White.B);
            Console.WriteLine(Color.Blue.R);
            Console.WriteLine(Color.Blue.V);
            Console.WriteLine(Color.Blue.B);
            Voiture v_v = new Voiture(2, 45);
            Console.WriteLine(v_v.GetInfosTechniques());
            Console.WriteLine(v_v.SeDeplacer());
            v_v.DegreSalisure = 10;
            Console.WriteLine(v_v.DegreSalisure);
            Station.LavageStation v_sta = Station.LavageFaible;
            v_sta(v_v);
            Console.WriteLine(v_v.DegreSalisure);
            v_sta = Station.LavageNormal;
            v_sta(v_v);
            Console.WriteLine(v_v.DegreSalisure);
            v_sta = Station.LavageIntense;
            v_sta(v_v);
            Console.WriteLine(v_v.DegreSalisure);
            v_v.DegreSalisure = 10;
            Console.WriteLine(v_v.DegreSalisure);
            v_sta = Station.LavageDeluxe;
            v_sta(v_v);
            Console.WriteLine(v_v.DegreSalisure);
        }
    }
}
