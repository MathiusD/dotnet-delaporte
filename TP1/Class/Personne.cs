﻿using System;

namespace Class
{
    /// <summary>
    /// Classe représentant une personne
    /// </summary>
    public class Personne
    {
        #region attributes
        private byte m_age;
        private String m_prenom;
        private String m_nom;
        #endregion
        
        public byte Age { get => m_age; }
        public String Prenom { get => m_prenom; }
        public String Nom { get => m_nom; }

        public Personne(String p_prenom, String p_nom, byte p_age)
        {
            if (p_prenom.Length < 2 || p_prenom == null)
            {
                throw new ArgumentException("Prenom incorrect");
            }
            if (p_nom.Length < 3 || p_nom == null)
            {
                throw new ArgumentException("Nom incorrect");
            }
            if (p_age >= 200)
            {
                throw new ArgumentException("Age incorrect");
            }
            m_age = p_age;
            m_nom = p_nom;
            m_prenom = p_prenom;
        }
    }
}
