﻿using System;
using Class;

namespace TP1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Personne p1;
            try
            {
                p1 = new Personne("", "", 230);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex);
            }
            p1 = new Personne("Blip", "Blap", 12);
            Console.WriteLine(p1.Nom);
            Console.WriteLine(p1.Prenom);
            Console.WriteLine(p1.Age);
        }
    }
}
