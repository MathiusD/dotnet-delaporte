﻿using System;

namespace Class
{
    public interface IOperation
    {
        bool State {get;}
        void Launch();
        void Stop();
    }
}
