using System;

namespace Class
{
    public static class Tools
    {
        public static void Swap<T>(ref T p_obj_1, ref T p_obj_2)
        {
            T v_obj_temp = p_obj_1;
            p_obj_1 = p_obj_2;
            p_obj_2 = v_obj_temp;
        }
    }
}
