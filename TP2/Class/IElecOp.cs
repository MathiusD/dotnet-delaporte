using System;

namespace Class
{
    public interface IElecOp : IOperation
    {
        byte Consumption {get;}
    }
}
