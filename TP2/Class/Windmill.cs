using System;

namespace Class
{
    public class Windmill : IOperation
    {
        private bool m_state;

        public bool State { get => m_state; }

        public Windmill()
        {
            m_state = false;
        }

        public void Launch()
        {
            m_state = true;
        }

        public void Stop()
        {
            m_state = false;
        }
    }
}
