using System;

namespace Class
{
    public static class ToolsIOperation<T> where T : IOperation
    {
        public static bool Launch(T p_ope)
        {
            p_ope.Launch();
            return p_ope.State;
        }

        public static bool Stop(T p_ope)
        {
            p_ope.Stop();
            return !p_ope.State;
        }
    }
}