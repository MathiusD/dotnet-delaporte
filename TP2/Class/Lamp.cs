using System;

namespace Class
{
    public class Lamp : IElecOp
    {
        public static byte ConsuptionOfLamp = 80;
        private byte m_consumption;

        public bool State { get
            {
                if (m_consumption == ConsuptionOfLamp)
                    return true;
                else
                    return false;
            }
        }

        public byte Consumption { get => m_consumption; }

        public Lamp()
        {
            m_consumption = 0;
        }

        public void Launch()
        {
            m_consumption = ConsuptionOfLamp;
        }

        public void Stop()
        {
            m_consumption = 0;
        }
    }
}
