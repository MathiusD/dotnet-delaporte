﻿using System;
using System.Timers;
using Class;

namespace TP2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Namespace et Using
            Timer v_timer = new Timer();
            //Typage
            uint v_a = 12;
            ulong v_b = 34;
            v_a = (uint) v_b;
            v_b = (ulong) v_a;
            //Switch
            int v_n;
            int.TryParse(Console.ReadLine(), out v_n);
            switch(v_n)
            {
                case 0:
                    Console.WriteLine("Exit.");
                    return;    
                //Boucles
                case 1:
                    //Exo 1
                    //Boucle de base
                    for(int v_i = 0; v_i < 10; v_i++)
                    {
                        Console.WriteLine(v_i);
                    }
                    //Boucle avec continue
                    for(int v_i = 0; v_i < 10; v_i++)
                    {
                        if (v_i < 7)
                        {
                            continue;
                        }
                        Console.WriteLine(v_i);
                    }
                    //Boucle avec break
                    for(int v_i = 0; v_i < 10; v_i++)
                    {
                        /*
                        //Syntaxe de continue
                        if (v_i > 4)
                        {
                            break;
                        }
                        */
                        if (v_i == 5)
                        {
                            break;
                        }
                        Console.WriteLine(v_i);
                    }
                    break;
                case 2:
                    //Exo 2
                    String text;
                    do
                    {
                        text = Console.ReadLine();
                    }
                    while(text.ToLower().StartsWith("a"));
                    break;
                case 3:
                    //Foreach
                    String[] v_j_sem = {"Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"};
                    foreach (String v_jour in v_j_sem)
                    {
                        Console.WriteLine(v_jour);
                    }
                    break;
                case 4:
                    //Exceptions
                    /*
                    //v1 pas top et HS
                    int v_int_compt = 0;
                    while (v_int_compt < 50)
                    {
                        try
                        {
                            int.Parse(Console.ReadLine());
                            v_int_compt++;
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Nombres uniquement !");
                        }
                        finally
                        {
                            Console.WriteLine("Vous avez saisi {0} nombre(s).", v_int_compt);
                        }
                    }
                    */
                    int v_int_compt = 0;
                    do
                    {
                        try
                        {
                            v_int_compt = v_int_compt + int.Parse(Console.ReadLine());
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Nombres uniquement !");
                        }
                        finally
                        {
                            Console.WriteLine("La somme fait {0}.", v_int_compt);
                        }
                    } while (v_int_compt < 50);
                    break;
                case 5:
                    //Interfaces
                    Lamp v_l = new Lamp();
                    Console.WriteLine(v_l.State);
                    Console.WriteLine(v_l.Consumption);
                    v_l.Launch();
                    Console.WriteLine(v_l.State);
                    Console.WriteLine(v_l.Consumption);
                    v_l.Stop();
                    Console.WriteLine(v_l.State);
                    Console.WriteLine(v_l.Consumption);
                    Windmill v_w = new Windmill();
                    Console.WriteLine(v_w.State);
                    v_w.Launch();
                    Console.WriteLine(v_w.State);
                    v_w.Stop();
                    Console.WriteLine(v_w.State);
                    break;
                case 6:
                    //Exo 6
                    //Swap
                    String v_c = "Blap";
                    String v_d = "Blip";
                    Console.WriteLine(v_c);
                    Console.WriteLine(v_d);
                    Tools.Swap<String>(ref v_c, ref v_d);
                    Console.WriteLine(v_c);
                    Console.WriteLine(v_d);
                    break;
                case 7:
                    //Gestion des IOperations
                    Lamp v_l_1 = new Lamp();
                    Console.WriteLine(ToolsIOperation<Lamp>.Launch(v_l_1));
                    Console.WriteLine(v_l_1.State);
                    Console.WriteLine(ToolsIOperation<Lamp>.Stop(v_l_1));
                    Console.WriteLine(v_l_1.State);
                    break;
                default:
                    Console.WriteLine("Invalid Selection.");
                    int.TryParse(Console.ReadLine(), out v_n);
                    break;
            }
        }
    }
}
